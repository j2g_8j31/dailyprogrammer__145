#!/usr/bin/python

from sys import argv

# read input
script, base, trunk, leaf = argv

base = int(base)

for i in range(1, (base + 1), 2):
    leaves = leaf * i
    print leaves.center(base, " ")

print "###".center(base, " ")
